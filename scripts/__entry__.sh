#!/bin/bash

function ok()        { echo -e "[\e[32mOK   \e[39m] $1"; }
function info()      { echo -e "[\e[36mINFO \e[39m] $1"; }
function error()     { echo -e "[\e[31mERROR\e[39m] $1" "1"; }
function errorExit() { echo -e "[\e[31mERROR\e[39m] $1" "1"; exit 1; }
function warn()      { echo -e "[\e[33mWARN \e[39m] $1"; }
function debug()     { echo -e "[\e[34mDEBUG\e[39m] $1"; }

info "Welcome to LaTeX-Docker"
info "Repository: https://gitlab.com/SaschaKr/latex-docker"
info "If you want to compile your tex file with lualatex - biber - lualatex - lualatex "
info "simply call lualatex.sh with <document>"
info "    <document> is the file without prefix "

debug "working directory $(pwd)"
debug "list files"
ls -la 

command=${1:-}
if [[ -n "$command" ]]; then
    echo "Call '$command' with arguments '${@:2}'. Now executing this command."
    "$command" "${@:2}"
fi
