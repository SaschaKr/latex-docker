#!/bin/bash

tmpdir=/tmp/.latex
mkdir -p $tmpdir
logfile=/tmp/.latex/log.txt
echo "" >$logfile #clean up

function do_log() {
   echo -e "$1"
   printf "$(date) $1\n" >>$logfile
}

function ok() { do_log "[\e[32mOK   \e[39m] $1"; }
function info() { do_log "[\e[36mINFO \e[39m] $1"; }
function error() { do_log "[\e[31mERROR\e[39m] $1" "1"; }
function errorExit() {
   do_log "[\e[31mERROR\e[39m] $1"
   do_log "[\e[31mERROR\e[39m] see error in file $logfile"
   if [[ "x$SHOW_LOG_ON_ERROR" == "xtrue" ]]; then
      error "Error file content "
      cat $logfile
   fi
   exit 1
}
function warn() { do_log "[\e[33mWARN \e[39m] $1"; }
function debug() { do_log "[\e[34mDEBUG\e[39m] $1"; }

function call_lualatex() {
   debug "[$STEP/$STEPS] start lualatex" &&
      lualatex --shell-escape $FILE >>$logfile 2>&1 &&
      ok "[$STEP/$STEPS] end   lualatex" ||
      errorExit "[$STEP/$STEPS] end   lualatex" &&
      STEP=$(($STEP + 1))
}

function call_biber() {
   debug "[$STEP/$STEPS] start biber" &&
      biber $FILE >>$logfile 2>&1 &&
      ok "[$STEP/$STEPS] end   biber" ||
      errorExit "[$STEP/$STEPS] end   biber" &&
      STEP=$(($STEP + 1))
}

function copy_result() {
   debug "[$STEP/$STEPS] start copy $FILE.pdf" &&
      cp "$tmpdir/$FILE.pdf" ./$FILE.pdf >>$logfile 2>&1 &&
      ok "[$STEP/$STEPS] end   copy" ||
      errorExit "[$STEP/$STEPS] end   copy" &&
      STEP=$(($STEP + 1))
}

function access_result() {
   # Change access to use the file in gitlab-ci
   debug "[$STEP/$STEPS] start access $FILE.pdf" &&
      chmod +777 ./$FILE.pdf &&
      ok "[$STEP/$STEPS] end   access" ||
      errorExit "[$STEP/$STEPS] end   access" &&
      STEP=$(($STEP + 1))
}

function clean_tmp() {
   debug "[$STEP/$STEPS] start  clean up $tmpdir" &&
      rm -rf $tmpdir/* &&
      ok "[$STEP/$STEPS] end    clean up" ||
      errorExit "[$STEP/$STEPS] end    clean up" &&
      STEP=$(($STEP + 1))
}

function set_parameter() {
   FAST="false"
   CLEAN="false"
   SHOW_LOG_ON_ERROR="false"

   while [[ $# -gt 0 ]]; do
      key="$1"

      case $key in
      -of | --only-lualatex)
         FAST="true"
         shift # past argument
         ;;
      -c | --clean-tmp)
         CLEAN="true"
         shift # past argument
         ;;
      -d | --directory)
         DIRECTORY="$2"
         shift # past argument
         shift # past value
         ;;
      -f | --file)
         FILE="$2"
         shift # past argument
         shift # past value
         ;;
      -s | --show-error)
         SHOW_LOG_ON_ERROR="true"
         shift
         ;;
      *)                    # unknown option
         POSITIONAL+=("$1") # save it in an array for later
         shift              # past argument
         ;;
      esac
   done

}

set_parameter "$@"

info "----------------------------"
info "== Welcome to lualatex.sh =="
info "----------------------------"
info "building file      '$FILE'"
info "in directory       '$DIRECTORY"
info "running fast       '$FAST'"
info "clean up           '$CLEAN'"
info "show log on error  '$SHOW_LOG_ON_ERROR'"
info ""
info "if any errors -> please see $logfile"
info "----------------------------"

cd $DIRECTORY &&
   ok "switched to directory '$DIRECTORY'" ||
   errorExit "no directory!"

# SETUP STEPS
STEPS=3
STEP=1
if [[ "x$FAST" == "xfalse" ]]; then STEPS=$(($STEPS + 3)); fi
if [[ "x$CLEAN" == "xtrue" ]]; then STEPS=$(($STEPS + 1)); fi

if [[ "x$CLEAN" == "xtrue" ]]; then
   clean_tmp
fi

call_lualatex

if [[ "x$FAST" == "xfalse" ]]; then
   call_biber
   call_lualatex
   call_lualatex
fi

#copy_result
access_result
