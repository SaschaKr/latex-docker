FROM ubuntu:20.10
LABEL MAINTAINER="Sascha Kreutz"

ARG DEBIAN_FRONTEND=noninteractive

# remove documentaries
ADD 01-nodoc.conf /etc/dpkg/dpkg.cfg.d/01-nodoc

RUN apt-get -y update \
 && apt-get -y upgrade \
 # && apt-get dist-upgrade -y \
 && apt-get -y install \
        software-properties-common \
 && apt-get -y install \
        git \
        wget \
        graphviz \
        python3-pip \
        texlive-full \
        biber \
 && apt-get -y clean \
 && rm -rf /var/lib/apt/lists/*

RUN pip install Pygments

RUN luaotfload-tool --update

COPY scripts /bin/

RUN mkdir -p /usr/share/fonts/myfonts &&\
    mkdir -p /tmp/.latex

VOLUME ["/data ", "/usr/share/fonts/external/"]

# RUN fc-cache -fv
WORKDIR /data

ENTRYPOINT ["/bin/__entry__.sh"]
