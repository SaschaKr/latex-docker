# LaTeX-Docker

Why another Docker container for LaTeX compilation?

Because I want to use following tools:
- lualatex
- biber
- custom-fonts (windows-fonts)

## From command line

```
docker run \
   -v <dir>:/data \
   -v <fonts>:/usr/share/fonts/external/ \
   saschakr/docker-latex:latest \
   lualatex.sh \ 
        --file <filename> \
        --directory <directory> \
        --show-error \
        --clean-tmp \
        --only-lualatex
 ```

with following parameters: 

| Parameter    | Value          | Example                                                                 |
| ------------ | -------------- | ----------------------------------------------------------------------- |
| `<dir>`      | Path to folder | `~/Documents/my_project` or <br>`C:/user/username/Documents/my_project` |
| `<fonts>`    | Path to fonts  | `~/.fonts` or <br> `C:/Windows/Fonts`                                   |
| `<filename>` | Filename       | `MyFileNameWithoutTexPrefix`                                            |

## From `gitlab-ci.yml`

```yml
compile_pdf:
  stage: build
  # from https://hub.docker.com/repository/docker/saschakr/docker-latex
  image: saschakr/docker-latex:latest      
  script:
    # if you use fonts you need to check them in
    - cp ./.gitlab/.fonts/* /usr/share/fonts/myfonts
    - cd Directory # optional
    - lualatex.sh MyFileNameWithoutTexPrefix
  artifacts:
    paths: 
      - Directory/MyFileNameWithoutTexPrefix.pdf  # instruct GitLab to keep the main.pdf file
      - /tmp/.latex/log.txt                       # keep log file
```
